package com.npaw.youbora.lib6.exoplayer2

import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.source.BehindLiveWindowException
import com.google.android.exoplayer2.trackselection.MappingTrackSelector
import com.google.android.exoplayer2.upstream.BandwidthMeter
import com.google.android.exoplayer2.upstream.HttpDataSource
import com.google.android.exoplayer2.util.Util
import com.npaw.youbora.lib6.Timer
import com.npaw.youbora.lib6.YouboraLog
import com.npaw.youbora.lib6.YouboraUtil
import com.npaw.youbora.lib6.adapter.PlayerAdapter
import kotlin.concurrent.schedule

open class Exoplayer2Adapter(player: ExoPlayer) : PlayerAdapter<ExoPlayer>(player),
    Player.Listener {

    /**
     * If you don't have an easy way to set the title in each mediaItem of a playlist, you probably
     * will want to update it in this callback calling
     * [com.npaw.youbora.lib6.plugin.Options.contentTitle].
     */
    var windowChangedListener: ExoplayerWindowChangedListener? = null

    /**
     * Sets a [BandwidthMeter] in order to allow the Adapter to get the throughput
     * ([BandwidthMeter.getBitrateEstimate]).
     */
    var bandwidthMeter: BandwidthMeter? = null

    private var currentWindowIndex = 0
    private var startPlayhead = 0.0
    private var lastPosition = 0.0

    private var joinTimer: Timer? = null

    private var customEventLogger: CustomEventLogger? = null
    private var customEventLoggerEnabled = false

    private var classError: String? = null
    private var errorMessage: String? = null
    private var errorCodeMetadata: String? = null

    private var skipNextBufferTimer: java.util.Timer? = null
    private var skipNextBuffer: Boolean = false
    private var skipStateChangedIdle: Boolean = false

    private var playerAnalyticsListener : PlayerAnalyticsListener? = null

    /**
     * If **true** the adapter will Ignore all [MediaItem] removals or replacements, default: **false**.
     */
    var ignoreMediaItemRemovals = false
        set(value) {
            field = value
            if (!value) ignoreNextMediaItemRemoval = value
        }

    /**
     *  Ignores next [MediaItem] removal or replacement.
     */
    private var ignoreNextMediaItemRemoval = false

    init { registerListeners() }

    override fun registerListeners() {
        super.registerListeners()
        addListenersToPlayer()
        joinTimer = createJoinTimer()
    }

    override fun unregisterListeners() {
        removeListenersFromPlayer()
        joinTimer = null
        super.unregisterListeners()
    }

    // Method to work easier on a non UI thread env
    protected open fun addListenersToPlayer() {
        player?.addListener(this)
        if (Util.SDK_INT > 23){
            playerAnalyticsListener = PlayerAnalyticsListener(this)
            playerAnalyticsListener?.let { player?.addAnalyticsListener(it) }
        }
    }

    protected open fun removeListenersFromPlayer() {
        player?.removeListener(this)
        playerAnalyticsListener?.let { player?.removeAnalyticsListener(it) }
    }

    /**
     * Ignores next [MediaItem] removal or replacement.
     *
     * Useful in case you want to ignored the next MediaItem removal/replacement that might be needed when creating a content fallback.
     */
    open fun ignoreNextMediaItemRemoval() {
        ignoreNextMediaItemRemoval = true
    }

    // Get methods
    open fun isExoPlayerPlayingAd(): Boolean { return player?.isPlayingAd ?: false }
    open fun getCurrentWindowIndex(): Int? { return player?.currentMediaItemIndex }

    override fun getPlayerVersion(): String {
        /* If we just try to access ExoPlayerLibraryInfo.VERSION we will always report the version
        of Exoplayer we're compiling the plugin with. As a workaround, we use reflection in order to
        get the class at runtime and access its VERSION field. If this fails, we report "unknown".*/
        val versionBuilder = StringBuilder("ExoPlayer2-")

        val versionField = ExoPlayerLibraryInfo::class.java.getDeclaredField("VERSION")
        versionBuilder.append(versionField.get(null) as String)

        return versionBuilder.toString()
    }

    override fun getPlayhead(): Double? {
        if (getIsLive()) return -1.0

        if (isExoPlayerPlayingAd()) return lastPosition

        /* We don't use player.contentPosition because we need to keep playhead when ads started,
        not when they are going to end. */
        player?.let { lastPosition = it.currentPosition / 1000.0 }

        return lastPosition
    }

    override fun getDuration(): Double? {
        val playerDuration = player?.duration

        return if (playerDuration == null || playerDuration == C.TIME_UNSET)
            super.getDuration()
        else
            playerDuration / 1000.0 // msec -> sec
    }

    override fun getThroughput(): Long? {
        var throughput = super.getThroughput()

        getBitrate()?.takeIf { it > 0 }?.let { throughput =
            bandwidthMeter?.bitrateEstimate ?: playerAnalyticsListener?.bitrateEstimate }

        return throughput
    }

    override fun getBitrate(): Long? {
        return player?.videoFormat?.bitrate?.toLong()
    }

    override fun getTotalBytes(): Long? {
        return if (customEventLoggerEnabled)
            customEventLogger?.totalBytesAccumulated
        else
            playerAnalyticsListener?.totalBytesAccumulated ?: super.getTotalBytes()
    }

    override fun getPlayrate(): Double {
        return player?.playbackParameters?.speed?.toDouble()
            .takeIf { !flags.isPaused } ?: super.getPlayrate()
    }

    override fun getRendition(): String? {
        return player?.videoFormat?.let {
            YouboraUtil.buildRenditionString(it.width, it.height, getBitrate()?.toDouble() ?: 0.0)
        }
    }

    override fun getResource(): String? {
        return player?.currentMediaItem?.localConfiguration?.uri.toString()
    }

    override fun getTitle(): String? {
        return player?.currentMediaItem?.mediaMetadata?.title.toString()
    }

    override fun getIsLive(): Boolean { return player?.isCurrentMediaItemLive ?: false }
    override fun getFramesPerSecond(): Double? { return player?.videoFormat?.frameRate?.toDouble() }
    override fun getDroppedFrames(): Int? { return customEventLogger?.droppedFrames ?: playerAnalyticsListener?.droppedFrames }
    override fun getLatency(): Double? { return player?.currentLiveOffset?.toDouble() }
    override fun getVersion(): String { return BuildConfig.VERSION_NAME + "-" + getPlayerName() }
    override fun getPlayerName(): String { return "ExoPlayer" }

    override fun onPlaybackStateChanged(playbackState: Int) {
        var debugStr = "onPlaybackStateChanged: "

        when (playbackState) {
            Player.STATE_BUFFERING -> {
                debugStr += "STATE_BUFFERING"
                stateChangedBuffering()
            }

            Player.STATE_ENDED -> {
                debugStr += "STATE_ENDED"
                stateChangedEnded()
            }

            Player.STATE_IDLE -> {
                debugStr += "STATE_IDLE"
                stateChangedIdle()
            }

            Player.STATE_READY -> {
                debugStr += "STATE_READY"
                stateChangedReady()
            }
        }

        YouboraLog.debug(debugStr)
    }

    override fun onPlayerError(error: PlaybackException) {
        classError = error.cause?.javaClass?.name
        /* The error message sometimes is very verbose, and has the resource in it, so we cannot
        use it as the error message. We send it as errorMetadata. Other times though, it is null. */

        // We use the cause's class name as the error code and description.
        errorMessage = error.message

        // An error code which identifies the cause of the playback failure.
        errorCodeMetadata = error.errorCodeName + ": ${error.errorCode}"

        if (error is ExoPlaybackException && error.type == ExoPlaybackException.TYPE_SOURCE) {
            when (error.sourceException) {
                is HttpDataSource.InvalidResponseCodeException -> invalidResponseCodeException(error)
                is HttpDataSource.HttpDataSourceException -> httpDataSourceException(error)
                is BehindLiveWindowException -> fireError(classError, errorMessage, errorCodeMetadata)
                else -> fireFatalError(classError, errorMessage, errorCodeMetadata)
            }
        } else {
            fireFatalError(classError, errorMessage, errorCodeMetadata)
        }

        skipStateChangedIdle = true
        YouboraLog.debug("onPlayerError: $error")
    }

    private fun invalidResponseCodeException(error: ExoPlaybackException) {
        val invalidResponseCodeException =
            error.sourceException as HttpDataSource.InvalidResponseCodeException
        fireError(
            classError,
            errorMessage,
            "Response message: ${invalidResponseCodeException.responseMessage}, " + errorCodeMetadata
        )
    }

    private fun httpDataSourceException(error: ExoPlaybackException) {
        val httpDataSourceException =
            error.sourceException as HttpDataSource.HttpDataSourceException

        when (httpDataSourceException.type) {
            HttpDataSource.HttpDataSourceException.TYPE_OPEN -> fireFatalError(
                classError,
                "OPEN - $errorMessage",
                errorCodeMetadata
            )

            HttpDataSource.HttpDataSourceException.TYPE_READ -> fireFatalError(
                classError,
                "READ - $errorMessage",
                errorCodeMetadata
            )

            HttpDataSource.HttpDataSourceException.TYPE_CLOSE -> fireFatalError(
                classError,
                "CLOSE - $errorMessage",
                errorCodeMetadata
            )
        }
    }

    override fun onPositionDiscontinuity(
        oldPosition: Player.PositionInfo,
        newPosition: Player.PositionInfo, reason: Int
    ) {
        YouboraLog.debug(
            "onPositionDiscontinuity: reason - $reason, " +
                    "oldPosition - ${oldPosition.positionMs}, " +
                    "newPosition - ${newPosition.positionMs}"
        )

        if (getCurrentWindowIndex() != currentWindowIndex) onDiscontinuityStop()
        if (reason == Player.DISCONTINUITY_REASON_REMOVE && !ignoreMediaItemRemovals) {
            if (!ignoreNextMediaItemRemoval) onDiscontinuityStop() else ignoreNextMediaItemRemoval = false
        }

        if (reason == Player.DISCONTINUITY_REASON_SEEK) fireSeekBegin()

        if ((reason == Player.DISCONTINUITY_REASON_AUTO_TRANSITION && plugin?.isAdBreakStarted == true)) skipNextBufferInsideTimePeriod()

        if (reason != Player.DISCONTINUITY_REASON_REMOVE) {
            startIfNoAdStarted()
            getPlayhead()?.let { startPlayhead = it }
            joinTimer?.start()
        }
    }

    private fun onDiscontinuityStop() {
        val stopParams = mutableMapOf<String, String>().apply { put("playhead", "-1") }
        fireStop(stopParams)
        getCurrentWindowIndex()?.let { windowChangedListener?.onExoplayerWindowChanged(it) }
    }

    protected open fun stateChangedBuffering() {
        if (player?.playWhenReady == true) startIfNoAdStarted()
        if (!isExoPlayerPlayingAd() && !skipNextBuffer) fireBufferBegin()
        skipNextBuffer = false
    }

    protected open fun stateChangedEnded() { fireStop() }
    protected open fun stateChangedIdle() {
        if (!skipStateChangedIdle)
            fireStop()

        skipStateChangedIdle = false
    }
    protected open fun stateChangedReady() {
        plugin?.takeIf { it.isAdBreakStarted }?.let {
            fireStart()
        }

        fireJoin()
        fireSeekEnd()
        fireBufferEnd()
    }

    override fun onPlayWhenReadyChanged(playWhenReady: Boolean, reason: Int) {
        if (!isExoPlayerPlayingAd()) {
            if (playWhenReady) {
                startIfNoAdStarted()
                fireResume()
            } else
                firePause()
        }

        YouboraLog.debug("onPlayWhenReadyChanged: playWhenReady - $playWhenReady, reason - $reason")
    }

    private fun startIfNoAdStarted() {
        if (!isExoPlayerPlayingAd()) fireStart()
    }

    fun skipNextBufferInsideTimePeriod(period: Long = 1000L) {
        skipNextBufferTimer?.cancel()
        skipNextBufferTimer = null
        skipNextBuffer = true
        YouboraLog.debug("Skip Next Buffer inside TimePeriod: $period")
        skipNextBufferTimer = java.util.Timer("skipNextBufferPeriodTask", false)
        skipNextBufferTimer?.schedule(period) { skipNextBuffer = false }
    }

    // Overridden Adapter methods
    override fun fireStart(params: MutableMap<String, String>) {
        // Save the windowIndex, aka. the playlist index
        getCurrentWindowIndex()?.let { currentWindowIndex = it }
        super.fireStart(params)
        joinTimer?.start()
    }

    override fun fireJoin(params: MutableMap<String, String>) {
        if (!isExoPlayerPlayingAd()) super.fireJoin(params)
    }

    override fun fireStop(params: MutableMap<String, String>) {
        super.fireStop(params)
        reset()
    }

    private fun createJoinTimer(): Timer {
        return Timer(object : Timer.TimerEventListener {
            override fun onTimerEvent(delta: Long) {
                plugin?.isAdBreakStarted?.takeIf { !it }?.let {
                    getPlayhead()?.takeIf { it > startPlayhead }?.let { joinAndStopTimer() }

                    if (getIsLive() && player?.playbackState == Player.STATE_READY)
                        joinAndStopTimer()
                }
            }
        }, 100)
    }

    private fun joinAndStopTimer() {
        if (flags.isStarted && !flags.isJoined) {
            fireJoin()
            YouboraLog.debug("Detected join time at playhead: ${getPlayhead()}")
            joinTimer?.stop()
        } else {
            joinTimer?.stop()
        }
    }

    private fun reset() {
        startPlayhead = 0.0
        lastPosition = 0.0
        playerAnalyticsListener?.reset()
    }

    interface ExoplayerWindowChangedListener {

        /**
         * This callback will be called by the adapter whenever a new media item (next item on the
         * playlist) is about to start playing.
         * @param newWindowIndex The index (starting at 0) of the newly loaded media.
         */
        fun onExoplayerWindowChanged(newWindowIndex: Int)
    }

    @Deprecated("The parameter trackSelector is ignored by EventLogger()", replaceWith = ReplaceWith("setCustomEventLogger()"))
    open fun setCustomEventLogger(trackSelector: MappingTrackSelector?) {
        player?.let {
            customEventLogger = CustomEventLogger(trackSelector)
            customEventLogger?.let(it::addAnalyticsListener)
            customEventLoggerEnabled = true
        }
    }

    open fun setCustomEventLogger() {
        player?.let {
            customEventLogger = CustomEventLogger()
            customEventLogger?.let(it::addAnalyticsListener)
            customEventLoggerEnabled = true
        }
    }

    override fun getUrlToParse(): String? {
        return if (customEventLoggerEnabled)
            customEventLogger?.urlToParse
        else
            playerAnalyticsListener?.urlToParse ?: super.getUrlToParse()
    }

    override fun getVideoCodec(): String? {
        return if (customEventLoggerEnabled)
            customEventLogger?.videoCodec
        else
            playerAnalyticsListener?.videoCodec ?: super.getVideoCodec()
    }

    override fun getAudioCodec(): String? {
        return if (customEventLoggerEnabled)
            customEventLogger?.audioCodec
        else
            playerAnalyticsListener?.audioCodec ?: super.getVideoCodec()
    }
}
