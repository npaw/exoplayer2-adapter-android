package com.npaw.youbora.lib6.exoplayer2

import com.google.android.exoplayer2.ExoPlayer

import com.npaw.youbora.lib6.YouboraLog

import com.teltoo.exoplayer.TeltooExoplayer
import com.teltoo.sdk.TeltooPlayerInterface

class Exoplayer2TeltooAdapter(val teltooFactory: TeltooExoplayer.Factory, player: ExoPlayer) :
        Exoplayer2Adapter(player) {

    var lastP2PTraffic: Long? = null
    var lastCDNTraffic: Long? = null
    var isP2pEnabled = false

    var teltooListener = object : TeltooPlayerInterface.EventListener {
        override fun onError(description: String?) {
            fireError(null, description, null)
        }

        override fun onState(connected: Boolean) { isP2pEnabled = connected }
        override fun onMessage(message: String?) { YouboraLog.debug("Teltoo message: $message") }

        override fun onStats(connectedPeers: Int, totalBytesReceived: Int, p2pBytesReceived: Int,
                             cacheBytes: Int, requestWithoutTeltoo: Int, p5: Int) {
            lastP2PTraffic = p2pBytesReceived.toLong()
            lastCDNTraffic = totalBytesReceived - p2pBytesReceived.toLong()
        }
    }

    init { teltooFactory.addListener(teltooListener) }

    override fun unregisterListeners() {
        teltooFactory.removeListener(teltooListener)
        super.unregisterListeners()
    }

    override fun getP2PTraffic(): Long? { return lastP2PTraffic }
    override fun getCdnTraffic(): Long? { return lastCDNTraffic }
    override fun getIsP2PEnabled(): Boolean { return isP2pEnabled }
}