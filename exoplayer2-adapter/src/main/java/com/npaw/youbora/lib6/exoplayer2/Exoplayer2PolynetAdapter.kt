package com.npaw.youbora.lib6.exoplayer2

import com.google.android.exoplayer2.ExoPlayer

import com.system73.polynet.android.sdk.PolyNet
import com.system73.polynet.android.sdk.PolyNetListener
import com.system73.polynet.android.sdk.core.metrics.PolyNetMetrics
import com.system73.polynet.android.sdk.exception.PolyNetException

class Exoplayer2PolynetAdapter(player: ExoPlayer, polyNet: PolyNet) : Exoplayer2Adapter(player) {

    var lastP2PTraffic: Long? = null
    var lastCDNTraffic: Long? = null
    var isP2pEnabled = false

    val listener = object : PolyNetListener {

        override fun onMetrics(p: PolyNetMetrics?) {
            isP2pEnabled = true
            lastP2PTraffic = p?.accumulatedP2pDownThroughput?.toLong()
            lastCDNTraffic = p?.accumulatedCdnDownThroughput?.toLong()
        }

        override fun onPlayBackStartedRequest(p: PolyNet?) { }
        override fun onBufferHealthRequest(p: PolyNet?) { }
        override fun onDroppedFramesRequest(p: PolyNet?) { }
        override fun onError(p: PolyNet?, ex: PolyNetException?) { }
    }

    init { polyNet.setListener(listener) }

    override fun getP2PTraffic(): Long? { return lastP2PTraffic }
    override fun getCdnTraffic(): Long? { return lastCDNTraffic }
    override fun getIsP2PEnabled(): Boolean { return true }
}