package com.npaw.youbora.lib6.exoplayer2

import com.google.android.exoplayer2.Format
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.analytics.AnalyticsListener
import com.google.android.exoplayer2.analytics.AnalyticsListener.EVENT_AUDIO_ENABLED
import com.google.android.exoplayer2.decoder.DecoderReuseEvaluation
import com.google.android.exoplayer2.source.LoadEventInfo
import com.google.android.exoplayer2.source.MediaLoadData
import com.npaw.youbora.lib6.YouboraLog

open class PlayerAnalyticsListener(private val adapter: Exoplayer2Adapter) : AnalyticsListener {

    var droppedFrames = -1
        private set

    var totalBytesAccumulated = 0L
        private set

    var bitrateEstimate = 0L
        private set

    var urlToParse: String? = null
        private set

    var videoCodec: String? = null
        private set

    var audioCodec: String? = null
        private set

    override fun onDroppedVideoFrames(
        eventTime: AnalyticsListener.EventTime,
        droppedFrames: Int,
        elapsedMs: Long
    ) {
        this.droppedFrames += droppedFrames
    }

    override fun onBandwidthEstimate(
        eventTime: AnalyticsListener.EventTime,
        totalLoadTimeMs: Int,
        totalBytesLoaded: Long,
        bitrateEstimate: Long
    ) {
        totalBytesAccumulated += totalBytesLoaded
        this.bitrateEstimate = bitrateEstimate
    }

    override fun onLoadStarted(
        eventTime: AnalyticsListener.EventTime,
        loadEventInfo: LoadEventInfo,
        mediaLoadData: MediaLoadData
    ) {
        urlToParse = loadEventInfo.uri.toString()
    }

    override fun onAudioInputFormatChanged(
        eventTime: AnalyticsListener.EventTime,
        format: Format,
        decoderReuseEvaluation: DecoderReuseEvaluation?
    ) {
        audioCodec = format.codecs
    }

    override fun onVideoInputFormatChanged(
        eventTime: AnalyticsListener.EventTime,
        format: Format,
        decoderReuseEvaluation: DecoderReuseEvaluation?
    ) {
        videoCodec = format.codecs
    }

    override fun onEvents(player: Player, events: AnalyticsListener.Events) {
        if (events.contains(EVENT_AUDIO_ENABLED) && adapter.flags.isJoined) {
            YouboraLog.debug("onEvents: EVENT_AUDIO_ENABLED")
            adapter.skipNextBufferInsideTimePeriod()
        }
    }

    fun reset() {
        droppedFrames = -1
        totalBytesAccumulated = 0L
        bitrateEstimate = 0L
        urlToParse = null
        videoCodec = null
        audioCodec = null
    }
}