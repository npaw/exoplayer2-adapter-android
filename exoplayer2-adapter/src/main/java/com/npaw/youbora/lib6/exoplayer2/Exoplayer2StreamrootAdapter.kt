package com.npaw.youbora.lib6.exoplayer2

import com.google.android.exoplayer2.ExoPlayer
import io.streamroot.lumen.delivery.client.core.LumenDeliveryClient
import io.streamroot.lumen.delivery.client.core.LumenMeshStats
import java.util.*
import kotlin.concurrent.fixedRateTimer

/**
 * Specific adapter to use along .<a href="https://streamroot.io/">Streamroot</a>
 *
 * This adapter for ExoPlayer allows you yo track specific P2P information like cdn and P2P traffic
 * plus all the events on a normal playback.
 *
 * To be able to use this adapter you will need to provide streamroot sdk, otherwise it will crash.
 */
open class Exoplayer2StreamrootAdapter(val deliveryClient: LumenDeliveryClient, player: ExoPlayer) :
        Exoplayer2Adapter(player) {

    var lastP2PTraffic: Long? = null
    var lastCDNTraffic: Long? = null
    var lastUploadTraffic: Long? = null
    var isP2pEnabled = false

    var statsTimer: Timer? = null

    override fun getP2PTraffic(): Long? { return lastP2PTraffic }
    override fun getCdnTraffic(): Long? { return lastCDNTraffic }
    override fun getIsP2PEnabled(): Boolean { return isP2pEnabled }
    override fun getUploadTraffic(): Long? { return lastUploadTraffic }

    override fun fireStart(params: MutableMap<String, String>) {
        statsTimer = fixedRateTimer("infoTimer", false, 0, 1000) {
            val stats : LumenMeshStats? = deliveryClient.getStats()
            stats?.let {
                lastP2PTraffic = it.p2p
                lastCDNTraffic = it.cdn
                lastUploadTraffic = it.upload
                isP2pEnabled = true
            }
        }

        super.fireStart(params)
    }

    override fun fireStop(params: MutableMap<String, String>) {
        super.fireStop(params)
        lastP2PTraffic = null
        lastCDNTraffic = null
        lastUploadTraffic = null
        isP2pEnabled = false
        statsTimer?.purge()
        statsTimer?.cancel()
        statsTimer = null
    }
}