package com.npaw.youbora.lib6.exoplayer2

import com.google.android.exoplayer2.C
import com.google.android.exoplayer2.Format
import com.google.android.exoplayer2.analytics.AnalyticsListener.EventTime
import com.google.android.exoplayer2.decoder.DecoderReuseEvaluation
import com.google.android.exoplayer2.source.LoadEventInfo
import com.google.android.exoplayer2.source.MediaLoadData
import com.google.android.exoplayer2.trackselection.MappingTrackSelector
import com.google.android.exoplayer2.util.EventLogger

class CustomEventLogger(trackSelector: MappingTrackSelector? = null) : EventLogger() {

    var droppedFrames = -1
        private set

    var totalBytesAccumulated = 0L
        private set

    var urlToParse: String? = null
        private set

    var videoCodec: String? = null
        private set

    var audioCodec: String? = null
        private set

    override fun onLoadStarted(eventTime: EventTime, loadEventInfo: LoadEventInfo,
                               mediaLoadData: MediaLoadData) {
        super.onLoadStarted(eventTime, loadEventInfo, mediaLoadData)

        /**
         * The Uri from which data is being read.
         * The uri will be identical to the one in dataSpec.uri unless redirection has occurred.
         * If redirection has occurred, this is the uri after redirection.
         */
        urlToParse = loadEventInfo.uri.toString()
    }

    override fun onAudioInputFormatChanged(eventTime: EventTime, format: Format,
                                           decoderReuseEvaluation: DecoderReuseEvaluation?) {
        //YouboraLog.debug("audioInputFormat = ${Format.toLogString(format)}")
        audioCodec = format.codecs
    }

    override fun onVideoInputFormatChanged(eventTime: EventTime, format: Format,
                                           decoderReuseEvaluation: DecoderReuseEvaluation?) {
        //YouboraLog.debug("videoInputFormat = ${Format.toLogString(format)}")
        videoCodec = format.codecs
    }

    override fun onDroppedVideoFrames(eventTime: EventTime, count: Int, elapsedMs: Long) {
        super.onDroppedVideoFrames(eventTime, count, elapsedMs)
        droppedFrames = count
    }

    override fun onBandwidthEstimate(eventTime: EventTime, totalLoadTimeMs: Int,
                                     totalBytesLoaded: Long, bitrateEstimate: Long) {
        super.onBandwidthEstimate(eventTime, totalLoadTimeMs, totalBytesLoaded, bitrateEstimate)
        totalBytesAccumulated += totalBytesLoaded
    }
}