package com.npaw.youbora.lib6.exoplayer2

import android.os.Handler
import android.os.Message
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.Player

open class Exoplayer2HandlerAdapter(player: ExoPlayer) : Exoplayer2Adapter(player), Handler.Callback {

    private var lastPlayhead: Double? = 0.0
    private var lastDuration: Double? = 0.0
    private var lastIsLive = false
    private var lastBitrate: Long? = -1L
    private var lastFramerate: Double? = null
    private var lastRendition: String? = null
    private var lastPlayrate = 1.0
    private var lastIsPlayingAd = false
    private var lastWindowIndex: Int? = 0

    private var exoHandler: Handler? = null

    private var localPlayer: Player? = null

    override fun addListenersToPlayer() {
        player?.let { p ->
            exoHandler = Handler(p.applicationLooper, this)
        }

        sendMessage(ADD_LISTENERS)
    }

    override fun removeListenersFromPlayer() {
        sendMessage(REMOVE_LISTENERS)
    }

    override fun getPlayhead(): Double? {
        sendMessage(GET_PLAYHEAD)
        return lastPlayhead
    }

    override fun getDuration(): Double? {
        sendMessage(GET_DURATION)
        return lastDuration
    }

    override fun getIsLive(): Boolean {
        sendMessage(GET_ISLIVE)
        return lastIsLive
    }

    override fun getBitrate(): Long? {
        sendMessage(GET_BITRATE)
        return lastBitrate
    }

    override fun getFramesPerSecond(): Double? {
        sendMessage(GET_FRAMERATE)
        return lastFramerate
    }

    override fun getRendition(): String? {
        sendMessage(GET_RENDITION)
        return lastRendition
    }

    override fun setCustomEventLogger() {
        sendMessage(SET_EVENT_LOGGER)
    }

    override fun getPlayrate(): Double {
        sendMessage(GET_PLAYRATE)
        return lastPlayrate
    }

    override fun isExoPlayerPlayingAd(): Boolean {
        sendMessage(GET_IS_PLAYING_AD)
        return lastIsPlayingAd
    }

    override fun getCurrentWindowIndex(): Int? {
        sendMessage(GET_WINDOW_INDEX)
        return lastWindowIndex
    }

    /** Handler methods **/

    private fun handlerIsLive() { lastIsLive = super.getIsLive() }
    private fun handlerPlayhead() { lastPlayhead = super.getPlayhead() }
    private fun handlerDuration() { lastDuration = super.getDuration() }
    private fun handlerPlayrate() { lastPlayrate = super.getPlayrate() }
    private fun handlerRendition() { lastRendition = super.getRendition() }
    private fun handlerBitrate() { lastBitrate = super.getBitrate() ?: -1L }
    private fun handlerFramerate() { lastFramerate = super.getFramesPerSecond() }
    private fun handlerIsPlayingAd() { lastIsPlayingAd = super.isExoPlayerPlayingAd() }
    private fun handlerSetEventLogger() { super.setCustomEventLogger() }
    private fun handlerCurrentWindowIndex() { lastWindowIndex = super.getCurrentWindowIndex() }
    private fun handlerAddListeners() {
        super.addListenersToPlayer()
        localPlayer = super.player
    }

    private fun handlerRemoveListeners() {
        super.removeListenersFromPlayer()
        exoHandler = null
        localPlayer?.removeListener(this)
        localPlayer = null
    }

    private fun sendMessage(method: Int) {
        val msg = Message.obtain().apply { what = method }
        exoHandler?.takeIf{ it.looper.thread.isAlive }?.sendMessage(msg)
    }

    override fun handleMessage(msg: Message): Boolean {
        when(msg.what) {
            GET_PLAYHEAD -> handlerPlayhead()
            GET_DURATION -> handlerDuration()
            GET_ISLIVE -> handlerIsLive()
            GET_BITRATE -> handlerBitrate()
            GET_RENDITION -> handlerRendition()
            GET_FRAMERATE -> handlerFramerate()
            GET_PLAYRATE -> handlerPlayrate()
            ADD_LISTENERS -> handlerAddListeners()
            REMOVE_LISTENERS -> handlerRemoveListeners()
            SET_EVENT_LOGGER -> handlerSetEventLogger()
            GET_IS_PLAYING_AD -> handlerIsPlayingAd()
            GET_WINDOW_INDEX -> handlerCurrentWindowIndex()
        }

        return true
    }

    companion object {
        const val GET_PLAYHEAD = 1
        const val GET_DURATION = 2
        const val GET_ISLIVE = 3
        const val GET_BITRATE = 4
        const val GET_RENDITION = 5
        const val GET_FRAMERATE = 6
        const val REMOVE_LISTENERS = 7
        const val ADD_LISTENERS = 8
        const val SET_EVENT_LOGGER = 9
        const val GET_PLAYRATE = 10
        const val GET_IS_PLAYING_AD = 11
        const val GET_WINDOW_INDEX = 12
    }
}