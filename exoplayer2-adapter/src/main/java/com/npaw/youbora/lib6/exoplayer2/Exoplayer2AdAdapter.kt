package com.npaw.youbora.lib6.exoplayer2

import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.source.BehindLiveWindowException
import com.google.android.exoplayer2.trackselection.MappingTrackSelector
import com.google.android.exoplayer2.upstream.HttpDataSource

import com.npaw.youbora.lib6.Timer
import com.npaw.youbora.lib6.YouboraLog
import com.npaw.youbora.lib6.YouboraUtil
import com.npaw.youbora.lib6.adapter.AdAdapter

open class Exoplayer2AdAdapter(player: ExoPlayer): AdAdapter<ExoPlayer>(player), Player.Listener {

    private var currentWindowIndex = 0
    private var startPlayhead = 0.0
    private var lastPosition = 0.0

    private var joinTimer: Timer? = null
    private var quartileTimer: Timer? = null

    private var customEventLogger: CustomEventLogger? = null
    private var customEventLoggerEnabled = false

    private var isFirstQuartileFired = false
    private var isSecondQuartileFired = false

    private var classError: String? = null
    private var errorMessage: String? = null

    init { registerListeners() }

    override fun registerListeners() {
        super.registerListeners()

        addListenersToPlayer()
        joinTimer = createJoinTimer()
        quartileTimer = createQuartileTimer()
    }

    override fun unregisterListeners() {
        removeListenersFromPlayer()
        joinTimer = null
        quartileTimer = null

        super.unregisterListeners()
    }

    // Method to work easier on a non UI thread env
    protected open fun addListenersToPlayer() { player?.addListener(this) }
    protected open fun removeListenersFromPlayer() { player?.removeListener(this) }

    // Get methods
    open fun getCurrentWindowIndex(): Int? { return player?.currentMediaItemIndex }

    override fun getPlayerVersion(): String {
        /* If we just try to access ExoPlayerLibraryInfo.VERSION we will always report the version
        of Exoplayer we're compiling the plugin with. As a workaround, we use reflection in order to
        get the class at runtime and access its VERSION field. If this fails, we report "unknown".*/
        val versionBuilder = StringBuilder("ExoPlayer2-")

        val versionField = ExoPlayerLibraryInfo::class.java.getDeclaredField("VERSION")
        versionBuilder.append(versionField.get(null) as String)

        return versionBuilder.toString()
    }

    override fun getPlayhead(): Double {
        player?.let { lastPosition = it.currentPosition / 1000.0 }

        return lastPosition
    }

    override fun getDuration(): Double? {
        val playerDuration = player?.duration

        return if (playerDuration == null || playerDuration == C.TIME_UNSET)
            super.getDuration()
        else
            playerDuration / 1000.0 // msec -> sec
    }

    override fun getBitrate(): Long? {
        return player?.videoFormat?.bitrate?.toLong()
    }

    override fun getRendition(): String? {
        return player?.videoFormat?.let {
            YouboraUtil.buildRenditionString(it.width, it.height, getBitrate()?.toDouble() ?: 0.0)
        }
    }

    override fun getResource(): String? {
        return player?.currentMediaItem?.localConfiguration?.uri.toString()
    }

    override fun getTitle(): String? {
        return player?.currentMediaItem?.mediaMetadata?.title.toString()
    }

    override fun getVersion(): String { return BuildConfig.VERSION_NAME + "-" + getPlayerName() }
    override fun getPlayerName(): String { return "ExoPlayer" }

    override fun onPlaybackStateChanged(playbackState: Int) {
        var debugStr = "onPlaybackStateChanged: "

        when (playbackState) {
            Player.STATE_BUFFERING -> {
                debugStr += "STATE_BUFFERING"
                stateChangedBuffering()
            }

            Player.STATE_ENDED -> {
                debugStr += "STATE_ENDED"
                stateChangedEnded()
            }

            Player.STATE_IDLE -> {
                debugStr += "STATE_IDLE"
                stateChangedIdle()
            }

            Player.STATE_READY -> {
                debugStr += "STATE_READY"
                stateChangedReady()
            }
        }

        YouboraLog.debug(debugStr)
    }

    override fun onPlayerError(error: PlaybackException) {
        classError = error.cause?.javaClass?.name
        /* The error message sometimes is very verbose, and has the resource in it, so we cannot
        use it as the error message. We send it as errorMetadata. Other times though, it is null. */

        // We use the cause's class name as the error code and description.
        errorMessage = error.message

        // Cast whether the error is an ExoPlaybackException Class type
        if (error is ExoPlaybackException && error.type == ExoPlaybackException.TYPE_SOURCE) {
            when (error.sourceException) {
                is HttpDataSource.InvalidResponseCodeException -> invalidResponseCodeException(error)
                is HttpDataSource.HttpDataSourceException -> httpDataSourceException(error)
                is BehindLiveWindowException -> fireError(classError, errorMessage)
                else -> fireFatalError(classError, errorMessage)
            }
        } else {
            fireFatalError(classError, errorMessage)
        }

        YouboraLog.debug("onPlayerError: $error")
    }

    private fun invalidResponseCodeException(error: ExoPlaybackException) {
        val invalidResponseCodeException = error.sourceException as HttpDataSource.InvalidResponseCodeException
        fireError(classError, errorMessage, "Response message: ${invalidResponseCodeException.responseMessage}")
    }

    private fun httpDataSourceException(error: ExoPlaybackException) {
        val httpDataSourceException = error.sourceException as HttpDataSource.HttpDataSourceException

        when (httpDataSourceException.type) {
            HttpDataSource.HttpDataSourceException.TYPE_OPEN -> fireFatalError(classError, "OPEN - $errorMessage")
            HttpDataSource.HttpDataSourceException.TYPE_READ -> fireFatalError(classError, "READ - $errorMessage")
            HttpDataSource.HttpDataSourceException.TYPE_CLOSE -> fireFatalError(classError, "CLOSE - $errorMessage")
        }
    }

    override fun onPositionDiscontinuity(oldPosition: Player.PositionInfo,
                                         newPosition: Player.PositionInfo, reason: Int) {
        YouboraLog.debug("onPositionDiscontinuity: reason - $reason, " +
                "oldPosition - ${oldPosition.positionMs}, " +
                "newPosition - ${newPosition.positionMs}")

        if (getCurrentWindowIndex() != currentWindowIndex) fireStop()

        fireStart()
        startPlayhead = getPlayhead()
        joinTimer?.start()
    }

    protected open fun stateChangedBuffering() {
        fireStart()
        fireBufferBegin()
    }

    protected open fun stateChangedEnded() { fireStop() }
    protected open fun stateChangedIdle() { fireStop() }
    protected open fun stateChangedReady() {
        fireJoin()
        fireBufferEnd()
    }

    override fun onPlayWhenReadyChanged(playWhenReady: Boolean, reason: Int) {
        if (playWhenReady)
            fireResume()
        else
            firePause()

        YouboraLog.debug("onPlayWhenReadyChanged: playWhenReady - $playWhenReady, reason - $reason")
    }

    // Overridden Adapter methods
    override fun fireStart(params: MutableMap<String, String>) {
        // Save the windowIndex, aka. the playlist index
        getCurrentWindowIndex()?.let { currentWindowIndex = it }
        quartileTimer?.start()
        super.fireStart(params)
    }

    override fun fireStop(params: MutableMap<String, String>) {
        super.fireStop(params)
        reset()
    }

    private fun createJoinTimer(): Timer {
        return Timer(object : Timer.TimerEventListener {
            override fun onTimerEvent(delta: Long) {
                if (getPlayhead() > startPlayhead) {
                    fireJoin()
                    YouboraLog.debug("Detected join time at playhead: ${getPlayhead()}")
                    joinTimer?.stop()
                }
            }
        }, 100)
    }

    private fun createQuartileTimer(): Timer {
        return Timer(object : Timer.TimerEventListener {
            override fun onTimerEvent(delta: Long) {
                getDuration()?.let { duration ->
                    if (getPlayhead() > duration * 0.25 && !isFirstQuartileFired) {
                        fireQuartile(1)
                        isFirstQuartileFired = true
                    } else if (getPlayhead() > duration * 0.5 && !isSecondQuartileFired) {
                        fireQuartile(2)
                        isSecondQuartileFired = true
                    } else if (getPlayhead() > duration * 0.75) {
                        fireQuartile(3)
                        quartileTimer?.stop()
                    }
                }
            }
        }, 100)
    }

    private fun reset() {
        startPlayhead = 0.0
        lastPosition = 0.0
        isFirstQuartileFired = false
        isSecondQuartileFired = false
    }

    open fun setCustomEventLogger() {
        player?.let {
            customEventLogger = CustomEventLogger()
            customEventLogger?.let(it::addAnalyticsListener)
            customEventLoggerEnabled = true
        }
    }
}
